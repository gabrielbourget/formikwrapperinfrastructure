import React from 'react';
// import LoginForm from './Forms/LoginForm/LoginForm';
// import RegistrationForm from './Forms/RegistrationForm/RegistrationForm';
import EnrollmentForm from './Forms/EnrollmentForm/EnrollmentForm';
import './App.css';

function App() {
  return (
    <div className="App">
      {/* <LoginForm/> */}
      {/* <RegistrationForm /> */}
      <EnrollmentForm/>
    </div>
  );
}

export default App;
