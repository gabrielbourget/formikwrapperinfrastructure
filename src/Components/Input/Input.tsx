import React from 'react';
import { css } from 'aphrodite';
import { Field, ErrorMessage } from 'formik';
import { styleGen } from './InputStyles';

interface IInputProps {
  label: string;
  name: string;
}

const Input: React.FC<IInputProps | any> = (props) => {
  const { label, name, ...rest } = props;
  const {
    formControl, error, labelStyle, formField
  } = styleGen();

  return (
    <div className={css(formControl)}>
      <label htmlFor={name} className={css(labelStyle)}>{label}</label>
      <Field
        className={css(formField)}
        id={name}
        name={name}
        {...rest}
      />
      <ErrorMessage
        name={name}
        render={(msg: string) => (
          <div className={css(error)}>{msg}</div>
        )}
      />
    </div>
  );
}

export default Input;