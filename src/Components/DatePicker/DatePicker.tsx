import React from 'react';
import { css } from 'aphrodite';
import { Field, ErrorMessage } from 'formik';
import DatePickerComponent from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import { styleGen } from './DatePickerStyles';

interface IDatePickerProps {
  label: string;
  name: string;
}

const DatePicker: React.FC<IDatePickerProps | any> = (props) => {
  const { label, name, ...rest } = props;
  const {
    formControl, error, labelStyle, formField,
  } = styleGen();

  return (
    <div className={css(formControl)}>
      <label htmlFor={name} className={css(labelStyle)}>{label}</label>
      <Field name={name}>
        {
          ({ field, form }: { field: any, form: any }) => {
            const { setFieldValue } = form;
            const { value } = field;

            return (
              <DatePickerComponent
                className={css(formField)}
                id={name}
                {...field}
                {...rest}
                selected={value}
                onChange={(changedValue) => setFieldValue(name, changedValue)}
              />
            );
          }
        }
      </Field>
      <ErrorMessage
        name={name}
        render={(msg: string) => (
          <div className={css(error)}>{msg}</div>
        )}
      />
    </div>
  );
}

export default DatePicker;
