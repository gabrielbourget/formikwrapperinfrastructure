import React from 'react';
import { css } from 'aphrodite';
import { Field, ErrorMessage } from 'formik';
import { styleGen } from './CheckboxGroupStyles';

interface ICheckboxGroupOption {
  key: string;
  value: string;
}

interface ICheckboxGroupProps {
  label: string;
  name: string;
  options: ICheckboxGroupOption[];
}

// - TODO: -> Create ability to span out options horizontally as an alternative to vertically.
// - TODO: -> Styling work so that many options cascade well.

const CheckboxGroup: React.FC<ICheckboxGroupProps | any> = (props) => {
  const { label, name, options, ...rest } = props;
  const {
    formControl, error, labelStyle, formField
  } = styleGen();

  return (
    <div className={css(formControl)}>
      <label htmlFor={name} className={css(labelStyle)}>{label}</label>
      <Field
        className={css(formField)}
        name={name}
        {...rest}
      >
        {
          ({ field }: { field: any }) => {
            
            return options.map((option: ICheckboxGroupOption) => {
              return (
                <React.Fragment key={option.key}>
                  <input
                    type="checkbox"
                    id={option.value}
                    {...field}
                    value={option.value}
                    checked={field.value.includes(option.value)}
                  />
                  <label htmlFor={option.value}>{option.key}</label>
                </React.Fragment>
              );
            });
          }
        }
      </Field>
      <ErrorMessage
        name={name}
        render={(msg: string) => (
          <div className={css(error)}>{msg}</div>
        )}
      />
    </div>
  );
}

export default CheckboxGroup;