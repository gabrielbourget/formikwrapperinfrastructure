import React from 'react';
import { css } from 'aphrodite';
import { Field, ErrorMessage } from 'formik';
import { styleGen } from './RadioGroupStyles';

interface IRadioGroupOption {
  key: string;
  value: string;
}

interface RadioGroupProps {
  label: string;
  name: string;
  options: IRadioGroupOption[];
}

// - TODO: -> Create ability to span out options horizontally as an alternative to vertically.
// - TODO: -> Styling work so that many options cascade well.

const RadioGroup: React.FC<RadioGroupProps | any> = (props) => {
  const { label, name, options, ...rest } = props;
  const {
    formControl, error, labelStyle, formField
  } = styleGen();

  return (
    <div className={css(formControl)}>
      <label htmlFor={name} className={css(labelStyle)}>{label}</label>
      <Field
        className={css(formField)}
        name={name}
        {...rest}
      >
        {
          ({ field }: { field: any }) => {
            
            return options.map((option: IRadioGroupOption) => {
              return (
                <React.Fragment key={option.key}>
                  <input
                    type="radio"
                    id={option.value}
                    {...field}
                    value={option.value}
                    checked={field.value === option.value}
                  />
                  <label htmlFor={option.value}>{option.key}</label>
                </React.Fragment>
              );
            });
          }
        }
      </Field>
      <ErrorMessage
        name={name}
        render={(msg: string) => (
          <div className={css(error)}>{msg}</div>
        )}
      />
    </div>
  );
}

export default RadioGroup;
