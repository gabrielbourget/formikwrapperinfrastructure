import React from 'react';
import {
  INPUT_FORM_ELEMENT, TEXT_AREA_FORM_ELEMENT,
  SELECT_FORM_ELEMENT, RADIO_FORM_ELEMENT,
  CHECKBOX_FORM_ELEMENT, DATE_FORM_ELEMENT,
  IFormikControlProps
} from './helpers';
import Input from '../Input/Input';
import TextArea from '../TextArea/TextArea';
import Select from '../Select/Select';
import RadioGroup from '../RadioGroup/RadioGroup';
import CheckboxGroup from '../CheckboxGroup/CheckboxGroup';
import DatePicker from '../DatePicker/DatePicker';

// - TODO: -> Remove 'any' return type once switch case filled out.
const FormikControl: React.FC<IFormikControlProps> = (props): any => {
  const { control, ...rest } = props;
  let mappedControl;
  switch (control) {
    case INPUT_FORM_ELEMENT:
      mappedControl = <Input {...rest} />;
      break;
    case TEXT_AREA_FORM_ELEMENT:
      mappedControl = <TextArea {...rest} />;
      break;
    case SELECT_FORM_ELEMENT:
      mappedControl = <Select {...rest} />;
      break;
    case RADIO_FORM_ELEMENT:
      mappedControl = <RadioGroup {...rest} />
      break;
    case CHECKBOX_FORM_ELEMENT:
      mappedControl = <CheckboxGroup {...rest} />
      break;
    case DATE_FORM_ELEMENT:
      mappedControl = <DatePicker {...rest} />
      break;
    default:
      console.error("[FormikControl]: That is not a valid form control type.");
      return null;
  }

  return mappedControl;
}

export default FormikControl;