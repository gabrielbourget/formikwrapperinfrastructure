export const INPUT_FORM_ELEMENT = "input";
export const TEXT_AREA_FORM_ELEMENT = "textarea";
export const SELECT_FORM_ELEMENT = "select";
export const RADIO_FORM_ELEMENT = "radio";
export const CHECKBOX_FORM_ELEMENT = "checkbox";
export const DATE_FORM_ELEMENT = "date";

interface ISelectOption {
  key: string,
  value: string,
}

export interface IFormikControlProps {
  control: 'input' | 'textarea' | 'select' | 'radio' | 'checkbox' | 'date';
  label: string;
  name: string;
  type?: string;
  options?: ISelectOption[];
}